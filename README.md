# EARLY WORK - NOT YET USABLE

<div align="center">
<p align="center"><img src="https://gitlab.com/troll-engine/create-sample/raw/master/logo.svg" align="center" width="350" height="250" alt="Project icon"></p>

<blockquote>
<p align="center">Creates a sample project for Troll Engine</p>
</blockquote>

<p align="center">
  <a href="https://gitlab.com/troll-engine/create-sample/commits/master" alt="pipeline status"><img src="https://gitlab.com/troll-engine/create-sample/badges/master/pipeline.svg" /></a>
  <a href="https://gitlab.com/troll-engine/create-sample/commits/master" alt="coverage report"><img src="https://gitlab.com/troll-engine/create-sample/badges/master/coverage.svg" /></a>
  <a href="https://www.npmjs.com/package/@troll-engine/create-sample" alt="version"><img src="https://badgen.net/npm/v/@troll-engine/create-sample" /></a>
  <a href="https://www.npmjs.com/package/@troll-engine/create-sample" alt="license"><img src="https://badgen.net/npm/license/@troll-engine/create-sample" /></a>
  <a href="https://www.npmjs.com/package/@troll-engine/create-sample" alt="downloads"><img src="https://badgen.net/npm/dt/@troll-engine/create-sample" /></a>
  <a href="https://discord.gg/ewwaSxv" alt="discord"><img src="https://discordapp.com/api/guilds/621750483098271745/widget.png" /></a>
</p>

</div>
</p>

```bash
npm init @troll-engine/sample
```

Creates a sample project. It extends the base project creator
by essentially copy pasting a series of extra source files into
the generated folder and passing a number of options to the config
file.

The sample project is a very simple little game showing you some
base functionality for the engine.

